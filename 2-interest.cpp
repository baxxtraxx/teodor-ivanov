#include <iostream>
#include <iomanip>
#include <string>
#include <limits>
#include <cmath>

using namespace std;

double compute_simple(double amount, double interest, int years)
{
    double v = amount*(interest/100);
    return amount + v*years;
}
double compute_compound(double amount, double interest, int years)
{
    double v = (1 + interest/100);
    return amount * pow(v, years);
}

int main()
{
    double amount, interest;
    int years;
    cin >> amount >> interest >> years;

    double simple = compute_simple(amount, interest, years);
    double compound = compute_compound(amount, interest, years);
    cout << simple << endl << compound << endl;

    return 0;
}
