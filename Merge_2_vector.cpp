#include <iostream>
#include <vector>
using namespace std;

int min(int a, int b)
{
	if (a < b) return a;
	return b;
}

vector<double> merge(vector<double> &A, vector<double> &B)
{
	int common_size = min(A.size(), B.size());
	vector<double> result;
	for (int i = 0; i < common_size; i++)
	{
		result.push_back(A[i]);
		result.push_back(B[i]);
	}

	for (int i = common_size; i < A.size(); i++)
	{
		result.push_back(A[i]);
	}

	for (int i = common_size; i < B.size(); i++)
	{
		result.push_back(B[i]);
	}

	return result;
}

void print_vector(vector<double> &A)
{
	for (int i = 0; i < A.size(); i++)
	{
		cout << A[i] << " ";
	}
	cout << endl;
}

int main()
{
	int N;
	cin >> N;
	vector<double> A(N);
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	int M;
	cin >> M; 
	vector<double> B(M);
	for (int i = 0; i < M; i++)
	{
		cin >> B[i];
	}

	vector<double> result = merge(A, B);
	print_vector(result);

	return 0;
}