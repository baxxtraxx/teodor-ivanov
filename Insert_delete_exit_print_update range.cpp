#include <iostream>
#include <string>
#include <vector>
using namespace std;

void print_vector(vector<double> &A)
{
	for (int i = 0; i < A.size(); i++)
	{
		cout << A[i] << " ";
	}
	cout << endl;
}

void update_range(vector<double> &A, int start_idx, int end_idx, double value)
{
	for (int i = start_idx; i <= end_idx; i++)
	{
		A[i] += value;
	}
}

int main()
{
	int N;
	cin >> N;
	vector<double> A(N);
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	string op;
	while (cin >> op)
	{
		if (op == "insert")
		{
			int index, value;
			cin >> index >> value;
			A.insert(A.begin() + index, value);
		}
		else if (op == "delete")
		{
			int index;
			cin >> index;
			A.erase(A.begin() + index);
		}
		else if (op == "update_range")
		{
			int begin, end;
			double value;
			cin >> begin >> end >> value;
			update_range(A, begin, end, value);
		}
		else if (op == "print")
		{
			print_vector(A);
		}
		else if (op == "exit")
		{
			break;
		}
	}


	return 0;
}