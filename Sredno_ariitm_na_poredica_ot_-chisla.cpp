#include <iostream>
using namespace std;
int main()
{
    int num;
    int sum = 0;
    int count = 0;
    double avrg = 0;

    cout << "Enter a number: ";
    cin >> num;
    count++;

    while (num < 0)
    {
        sum += num;

        cout << "Enter a number: ";
        cin >> num;
        count++;
    }

    if (count == 1)
    {
        cout << "No valid input data" << endl;
    }
    else
    {
        avrg = sum / (count - 1.0);
        cout << "The result is: " << avrg;
    }
}
