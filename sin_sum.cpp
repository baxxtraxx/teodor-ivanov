//Nikola Dimov
//F75329

#include <iostream>

using namespace std;

int main()
{
    //nomerator e chisliten, a denominator e znamenatel
    double sum = 0, fraction, numerator, numeratorInput, denominator = 1;
    int index = 1, precision, sign = 1, denominatorIndex = 1;

    cin >> numeratorInput >> precision;

    fraction = numerator = numeratorInput;

    while(index <= precision)
    {
        //Sum takes the first fraction which is equal to the inputed numerator
        sum += fraction;

        //Numerator will be increased by calculating the power of the inputed numerator
        numerator *= numeratorInput * numeratorInput;
        //Factorial will be calculated by adding two new number which index is increased by two every period of thy cycle
        denominator *= (denominatorIndex + 1) * (denominatorIndex + 2);
        //Sign will be changed every time from + to -
        sign *= -1;

        //Each fraction will be built separately using the sign, numerator and denominator
        fraction = sign * (numerator / denominator);

        index++;
        denominatorIndex += 2;

        //Debugging messages
        //cout << endl << "Numerator: " << numerator << endl << "Denominator: " << denominator << endl << "Fraction: " << fraction << endl << "Sign: " << sign << endl << "Sum: " << sum << endl;
    }

    cout << sum;
}
