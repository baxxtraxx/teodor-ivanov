#include <iostream>
#include <string>
using namespace std;

void print_grade(string& faculty_number,double grade )

{
    faculty_number+='\t';
    if(grade>0&&grade<=2.49)
    {
        faculty_number+='F';
        cout<<faculty_number;
    }
    else if(grade>=2.50&&grade<=2.99)
    {
        faculty_number+='E';
        cout<<faculty_number;
    }
    else if(grade>=3.00&&grade<=3.49)
    {
        faculty_number+='D';
        cout<<faculty_number;
    }
    else if(grade>=3.50&&grade<=4.49)
    {
        faculty_number+='C';
        cout<<faculty_number;
    }
    else if(grade>=4.50&&grade<=5.49)
    {
        faculty_number+='B';
        cout<<faculty_number;
    }
    else if(grade>=5.50&&grade<=6.00)
    {
        faculty_number+='A';
        cout<<faculty_number;
    }
}

int main()
{
    string faculty_number;
    double grade;
    while(cin!=0)
    {cin>>faculty_number>>grade;
    }
        print_grade(faculty_number,grade);


    return 0;
}
