#include <string>
#include <iostream>
using namespace std;

bool is_number(string str)
{
	for (int i = 0; i < str.size(); i++)
	{
		if ((str[i] < '0' || str[i] > '9') && str[i] != '.')
		{
			return false;
		}
	}
	return true;
}

void modify(string& str)
{
	if (is_number(str)) str.append(" [OK]");
	else str.append(" [FAIL]");
}

int main()
{
	string s;

	getline(cin, s);

	modify(s);

	cout << s << endl;

	return 0;
}