#include <iostream>

using namespace std;

double fDescent(double x1, double x2, double numBeforeX, double numAfterX)
{
    double x, y;

    if(x1 > x2)
    {
        x = x1;
    }
    else
    {
        x = x2;
    }

    do
    {
        x++;
        y = numBeforeX * x + numAfterX;
    }
    while (y > 0);

    return x;
}

double fAscent(double x1, double x2, double numBeforeX, double numAfterX)
{
    double x, y;

    if(x1 < x2)
    {
        x = x1;
    }
    else
    {
        x = x2;
    }

    do
    {
        x--;
        y = numBeforeX * x + numAfterX;
        //debug
        //cout << endl << endl << x << "\t" << y << endl << endl;
    }
    while (y > 0);

    return x;
}

int main()
{
    double x1, x2, y1, y2, numBeforeX, numAfterX;

    cout << "(x1; y1)";
    cin >> x1 >> y1;

    cout << "(x2; y2)";
    cin >> x2 >> y2;

    if(x1<0 || x2<0 || y1<0 || y2<0)
    {
        cout << "This is not a linear function in first quarter!";
        return 0;
    }

    numBeforeX = (y2 - y1) / (x2 - x1);
    numAfterX = y1 - numBeforeX * x1;

    //debug
    //cout << endl << endl << numBeforeX << " " << numAfterX << endl;

    if(numBeforeX < 0)
    {
        cout << fDescent(x1, x2, numBeforeX, numAfterX);
    }
    else
    {
        cout << fAscent(x1, x2, numBeforeX, numAfterX);
    }
}
