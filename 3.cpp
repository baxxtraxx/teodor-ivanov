/***
FN:F75201
PID:3
GID:2
*/

#include <iostream>
#include <string>
#include <vector>


using namespace std;

void shift_left(vector<double>& a)
{
	double first = a.front();
	a.erase(a.begin());
	a.push_back(first);
}
void reverse(vector<double>& a)
{
	vector<double>::iterator front = a.begin();
	vector<double>::iterator back = a.end();
	while ((front != back) && (front != --back)) {
        iter_swap(front++, back);
    }
}

void shift_right(vector<double>& a)
{
	double last = a.back();
	double first = a.front();
	a.pop_back();
	a.insert(a.begin(),last);
}

void add(vector <double> &a)
{
    int position;
	double value;
	cin>>position;
	cin>>value;
	a.insert(a.begin()+position, value);
}
int main()
{
	int n;
    cin>>n;
    vector <double> a(n);
    for(int i = 0;i<n;i++)
    {
        cin>>a[i];
    }
    string operation;
    while(cin>>operation)
    {
        if(operation == "add 2-4")
        {
            if(a.size() == 0)
            {
            cout<<"Can not add to empty vector"<<endl;
            continue;
            }
           for(int i = 0;i<a.size();i++)
           {
            a.push_back(i);
            add(a);
            cout<<a[i];
           }

        }
        else if(operation == "shift_right")
        {
            for(int i= 0;i<a.size();i++)
            {
            if(a.size()== 0)
            {
            cout<<"Can not shift empty vector"<<endl;
            continue;
            }
            a.pop_back();
            shift_right(a);
            cout<<a[i]<<" ";
            cout<<endl;
            }

        }
        else if(operation == "shift_left")
        {
            for(int i = 0;i<n;i++)
            {
            a.push_back(i);
            shift_left(a);
            cout<<a[i]<<" ";
            }
        }
        else if(operation =="reverse")
        {
            for(int i = 0;i<a.size();i++)
            {
            reverse(a);
            cout<<a[i]<<" ";
            }
        }
        else if (operation == "print")
		{
			for (int i = 0; i < a.size(); i++)
			{
				cout << a[i] << " ";
			}
			cout << endl;
		}
		else if(operation == "exit")
        {
            return 0;
        }

    }

	return 0;
}

