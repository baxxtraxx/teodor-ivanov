#include <iostream>

using namespace std;

double f(double fArgValue)
{
    double fValue = fArgValue*fArgValue*fArgValue+3*fArgValue-5;
    return fValue;
}

int main()
{
    double lStep, rStep, midStep, fMismatch;
    //int indexIteration = 1, maxIteration;

    cout << "Vuvedete intervala na funkciqta v x: ";
    cin >> lStep >> rStep;

    if(f(lStep)*f(rStep)>0)
    {
        cout << "Ili nqkoq ot granicite e koren ili vuobshte nqma koren ili sa poveche ot edin...";
        return 0;
    }

    cout << "Vuvedete dopustimoto nesyotvetstvie vuv vrushtanata st-st na f: ";
    cin >> fMismatch;

    //cout << "Vuvedete maksimalnata iteraciq ";
    //cin >> maxIteration;

    //while(indexIteration <= maxIteration || ((rStep-lStep) >= 2*fMismatch))
    while((rStep-lStep) >= 2*fMismatch)
    {
        midStep = (lStep+rStep)/2;

        if(f(lStep)*f(midStep)<0)
        {
            rStep = midStep;
        }
        else
        {
            lStep = midStep;
        }

        if(f(midStep) == 0)
        {
            cout << "Tochniqt koren e: " << midStep;
            return 0;
        }

        //indexIteration++;
    }

    cout << "Priblizitelen koren na uravnenieto e: " << midStep;
}
