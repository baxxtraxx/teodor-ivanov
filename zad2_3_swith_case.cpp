#include <vector>
#include <iostream>
#include <string>
using namespace std;

void add(vector<double>& a)
{
	int position;
	double value;
	cin>>position;
	cin>>value;
	a.insert(a.begin()+position, value);
}

void remove(vector<double>& a)
{
	int position;
	cin>>position;
	a.erase(a.begin()+position);
}

void shift_left(vector<double>& a)
{
	double first = a.front();
	a.erase(a.begin());
	a.push_back(first);
}

void input_vector(vector<double>& a)
{
	int n;
	cin>>n;
	double input;
	for(int i = 0;i<n;i++)
	{
		cin>>input;
		a.push_back(input);
	}
}

void print(vector<double>& a)
{
	for(int i = 0;i<a.size();i++)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;
}

void reverse(vector<double>& a)
{
	vector<double>::iterator front = a.begin();
	vector<double>::iterator back = a.end();
	while ((front != back) && (front != --back)) {
        iter_swap(front++, back);
    }
}

void menu(vector<double>& a)
{
	int choise;
	bool exit = false;
	while(!exit) {

		cout <<"Choose an option"<<endl;

		cin >> choise;
		switch(choise)
		{
			case 1:
				add(a);
				break;
			case 2:
				remove(a);
				break;
			case 3:
				shift_left(a);
				break;
			case 4:
				reverse(a);
				break;
			case 5:
				print(a);
				break;
			case 6:
				exit = true;
				break;
		}
	}

}

int main()
{

	vector<double> a;
	input_vector(a);
	menu(a);

	return 0;
}

