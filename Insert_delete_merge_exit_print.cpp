#include <iostream>
#include <string>
#include <vector>
using namespace std;

void print_vector(vector<double> &A)
{
	for (int i = 0; i < A.size(); i++)
	{
		cout << A[i] << " ";
	}
	cout << endl;
}

int min(int a, int b)
{
	if (a < b) return a;
	return b;
}

vector<double> merge(vector<double> &A, vector<double> &B)
{
	int common_size = min(A.size(), B.size());
	vector<double> result;
	for (int i = 0; i < common_size; i++)
	{
		result.push_back(A[i]);
		result.push_back(B[i]);
	}

	for (int i = common_size; i < A.size(); i++)
	{
		result.push_back(A[i]);
	}

	for (int i = common_size; i < B.size(); i++)
	{
		result.push_back(B[i]);
	}

	return result;
}

int main()
{
	int N;
	cin >> N;
	vector<double> A(N);
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	string op;
	while (cin >> op)
	{
		if (op == "insert")
		{
			int index, value;
			cin >> index >> value;
			A.insert(A.begin() + index, value);
		}
		else if (op == "delete")
		{
			int index;
			cin >> index;
			A.erase(A.begin() + index);
		}
		else if (op == "merge")
		{
			int M;
			cin >> M;
			vector<double> B(M);
			for (int i = 0; i < M; i++)
			{
				cin >> B[i];
			}
			A = merge(A, B);
		}
		else if (op == "print")
		{
			print_vector(A);
		}
		else if (op == "exit")
		{
			break;
		}
	}


	return 0;
}