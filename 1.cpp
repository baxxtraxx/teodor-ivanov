/***
FN:F75201
PID:1
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

double lucky_sum(vector<double>& a,vector<double>& b,int n)
{
	double result = a[0]*b[0];
	for(int i = 1;i<a.size();i++)
	{
		result -= a[i] * b[i];
	}
	return result;
}

int main()
{
	int n;
	cin>>n;
	vector<double> a,b;
	double input;
	for(int i = 0;i<n;i++)
	{
		cin>>input;
		a.push_back(input);
	}
	for(int i = 0;i<n;i++)
	{
		cin>>input;
		b.push_back(input);
	}
	cout<<lucky_sum(a,b,n)<<endl;;


	return 0;
}
