#include <iostream>
#include <string>
#include <vector>
using namespace std;

string print_grade(string faculty_number, double grade )
{
	faculty_number += '\t';
	if(grade>0&&grade<=2.49)
	{
		faculty_number += 'F';
	} else if(grade>=2.50&&grade<=2.99)
	{
		faculty_number += 'E';
	} else if(grade>=3.00&&grade<=3.49)
	{
		faculty_number += 'D';
	} else if(grade>=3.50&&grade<=4.49)
	{
		faculty_number += 'C';
	} else if(grade>=4.50&&grade<=5.49)
	{
		faculty_number += 'B';
	} else if(grade>=5.50&&grade<=6.00)
	{
		faculty_number += 'A';
	}
	return faculty_number;
}



int main()
{
	vector<string> faculty_numbers;
	vector<double> grades;
    string faculty_number = "";
    double grade = 0.0;
	
    while(cin>>faculty_number)
	{
		if(!faculty_number.compare("end")) 
		{
			break;	
		}
		cin>>grade;
		faculty_numbers.push_back(faculty_number);
		grades.push_back(grade);
    }
	
	for(unsigned int i = 0; i < faculty_numbers.size(); i++) {
		cout<<print_grade(faculty_numbers[i],grades[i])<<endl;
	}

    return 0;
}