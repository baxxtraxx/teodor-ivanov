#include <iostream>
#include <vector>
using namespace std;

double lucky_quotient(vector<double> &A, vector<double> &B)
{
	double result = 0;
	for (int i = 1; i < A.size(); i += 2)
	{
		result += A[i] / B[i];
	}
	return result;
}

int main()
{
	int N;
	cin >> N;

	vector<double> A(N);
	vector<double> B(N);

	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	for (int i = 0; i < N; i++)
	{
		cin >> B[i];
	}

	double result = lucky_quotient(A, B);
	cout << result << endl;

	return 0;
}