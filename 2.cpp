/***
FN:F75201
PID:2
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void shift_right(vector<double>& a)
{
	double last = a.back();
	a.pop_back();
	a.insert(a.begin(),last);
}

int main()
{
	int n;
	cin>>n;
	vector<double> a,b;
	double input;
	for(int i = 0;i<n;i++)
	{
		cin>>input;
		a.push_back(input);
	}
	shift_right(a);
	for(int i = 0;i<n;i++)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;


	return 0;
}
