#include <iostream>
#include <string>
#include <vector>
using namespace std;

void print_vector(vector<double> &A)
{
	for (int i = 0; i < A.size(); i++)
	{
		cout << A[i] << " ";
	}
	cout << endl;
}

vector<double> values_at(vector<double> &values, vector<int> &positions)
{
	vector<double> result;

	for (int i = 0; i < positions.size(); i++)
	{
		double value = values[positions[i]];
		result.push_back(value);
	}

	return result;
}

int main()
{
	int N;
	cin >> N; 
	vector<double> A(N);
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	string op;
	while (cin >> op)
	{
		if (op == "insert")
		{
			int index, value;
			cin >> index >> value;
			A.insert(A.begin() + index, value);
		}
		else if (op == "delete")
		{
			int index;
			cin >> index;
			A.erase(A.begin() + index);
		}
		else if (op == "values_at")
		{
			int M;
			cin >> M;
			vector<int> positions(M);
			for (int i = 0; i < M; i++)
			{
				cin >> positions[i];
			}
			A = values_at(A, positions);
		}
		else if (op == "print")
		{
			print_vector(A);
		}
		else if (op == "exit")
		{
			break;
		}
	}


	return 0;
}