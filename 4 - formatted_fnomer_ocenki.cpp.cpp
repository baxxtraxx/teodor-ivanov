#include <string>
#include <iostream>
#include <iomanip>
using namespace std;
 
void print_column(double value, int col_width)
{
	cout << setw(col_width) << setprecision(2) << fixed << value;
}

void print_column(string value, int col_width)
{
	cout << setw(col_width) << value;
}

void print_newline()
{
	cout << endl;
}

void print_data_row(const string& fn, double g1, double g2, double g3, double g4)
{
	print_column(fn, 10);
	print_column(g1, 6);
	print_column(g2, 6);
	print_column(g3, 6);
	print_column(g4, 6);
	print_newline();
}

int main()
{ 
	string fn;
	double grade1, grade2, grade3, grade4;
	print_column("Faculty No", 10);
	print_column("G1", 6);
	print_column("G2", 6);
	print_column("G3", 6);
	print_column("G4", 6);
	print_newline();
	while (cin >> fn >> grade1 >> grade2 >> grade3 >> grade4)
	{
		print_data_row(fn, grade1, grade2, grade3, grade4);
	}

	return 0;
}