#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
#include <limits>
using namespace std;

double prosta_lihva(double suma, double lihva, int godini)
{
double v = suma*(lihva/100);
return suma+ v*godini;
}
double slojna_lihva(double suma,double lihva,int godini)
{
double v = (1 + lihva/100);
return suma * pow(v,godini);
}

int main ()
{
 double suma,lihva;
 int godini;
 cout<< "Enter amount,interest and years"<<endl;
 cin>>suma;
 cin>>lihva;
 cin>>godini;
if(suma<=0)
{
    cout<<"Error,negative amount"<<endl;
}
if(lihva<=0)
{
    cout<<"Error,negative interest"<<endl;
}
if(godini<=0)
{
    cout<<"Error,negative years"<<endl;
}
double prosta = prosta_lihva(suma,lihva,godini);
double slojna = slojna_lihva(suma,lihva,godini);

cout<<prosta<<endl;
cout<<slojna<<endl;
return 0;

}
