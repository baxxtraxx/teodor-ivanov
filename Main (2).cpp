#include <iostream>
#include <string>
#include <vector>
using namespace std;

double computeSum(vector<double> numbers)
{
	double result = 0;
	for (int i = 0; i < numbers.size(); i++)
	{
		result += numbers[i];
	}
	return result;
}

double computeAvg(vector<double> numbers)
{
	return computeSum(numbers) / numbers.size();
}

double computeMin(vector<double> numbers)
{

	double minValue = numbers[0];
	for (int i = 0; i < numbers.size(); i++)
	{
		if (numbers[i] < minValue)
		{
			minValue = numbers[i];
		}
	}
	return minValue;
}

double computeMax(vector<double> numbers)
{
	double maxValue = numbers[0];
	for (int i = 0; i < numbers.size(); i++)
	{
		if (numbers[i] > maxValue)
		{
			maxValue = numbers[i];
		}
	}
	return maxValue;
}

int main()
{
	int n;
	cin >> n; // broi elementi

	vector<double> numbers(n); // deklarirame vector s n elementa
	for (int i = 0; i < n; i++)
	{
		cin >> numbers[i];
	}

	string op; 
	while (cin >> op) //chetem operacii do kraq na vhoda
	{
		if (op == "sum")
		{
			if (numbers.size() == 0)
			{
				cout << "Can not compute sum on empty vector" << endl;
				continue; // ...next operation
			}
			cout << computeSum(numbers) << endl;;
		}
		else if (op == "avg")
		{
			if (numbers.size() == 0)
			{
				cout << "Can not compute avg on empty vector" << endl;
				continue; // ...next operation
			}
			cout << computeAvg(numbers) << endl;;
		}
		else if (op == "min")
		{
			if (numbers.size() == 0)
			{
				cout << "Can not compute min on empty vector" << endl;
				continue; // ...next operation
			}
			cout << computeMin(numbers) << endl;;
		}
		else if (op == "max")
		{
			if (numbers.size() == 0)
			{
				cout << "Can not compute max on empty vector" << endl;
				continue; // ...next operation
			}
			cout << computeMax(numbers) << endl;;
		}
		else if (op == "ins")
		{
			int a;
			cin >> a;
			numbers.push_back(a);
		}
		else if (op == "del")
		{
			if (numbers.size() == 0)
			{
				cout << "Can not delete from empty vector" << endl;
				continue;
			}

			int i;
			cin >> i;
			i--; // trqbva da izvadim 1 ot indexa poneje vektora se indexira ot 0

			if (i < 0 || i >= numbers.size())
			{
				cout << "Index to delete is out of vector range (1, " << numbers.size() << ")" << endl;
				continue;
			}
			numbers.erase(numbers.begin() + i);
		}
		else if (op == "print")
		{
			cout << "numbers = ";
			for (int i = 0; i < numbers.size(); i++)
			{
				cout << numbers[i] << " ";
			}
			cout << endl;
		}
		else
		{
			cout << "Unknown operation... exiting...";
		}
	}
	return 0;
}