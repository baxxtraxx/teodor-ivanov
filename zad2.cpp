#include <iostream>
using namespace std;

double parameter(int num)
{
	int sum = 0;
  	while ( num > 0 ) {
		sum += num % 10;
		num /= 10;
	}
    return sum;

}

int main()
{
	int a;
	cin>>a;
    if(a>0)
    {
		int sum = parameter(a);
		cout<<sum<<endl;
    } else {
		cout<<"Invalid number"<<endl;
	}

	return 0;

}

