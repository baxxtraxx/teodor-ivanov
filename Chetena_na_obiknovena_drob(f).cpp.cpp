#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

class Fraction{
public:
	Fraction(int n, int d)
	{
		nominator = n;
		denominator = d;
	}

	int nominator;
	int denominator;
};

Fraction read()
{
	int n, d;
	cout << "Enter fraction (two numbers separated by space):" << endl;
	cin >> n >> d;

	Fraction result(n, d);
	return result;
}

Fraction add(const Fraction& f1, const Fraction& f2)
{
	int denominator = f1.denominator * f2.denominator;
	int nominator = f2.denominator * f1.nominator + f1.denominator * f2.nominator;
	//normalize

	Fraction result(nominator, denominator);
	return result;
} 

void print(const Fraction& f)
{
	cout << f.nominator << "/" << f.denominator;
}

int GCD(int a, int b)
{
	while (b != 0)
	{
		int t = b;
		b = a % b;
		a = t;
	}
	return a;
} 

Fraction normalize(const Fraction& f)
{
	int gcd = GCD(f.nominator, f.denominator);
	int n = f.nominator / gcd;
	int d = f.denominator / gcd;

	Fraction result(n, d);
	return result;
}

int main()
{
	Fraction f1(5, 3);
	Fraction f2(-6, 3);

	Fraction f3 = add(f1, f2);

	cout << "The result is ";
	print(f3);
	cout << endl;
	
	Fraction f4 = normalize(f3);
	cout << "The normalized result is ";
	print(f4);
	cout << endl;

	//f1 = readFraction();
	//f2 = readFraction();

	return 0;
}