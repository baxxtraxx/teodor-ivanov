#include <iostream>
using namespace std;

int main()
{
    int num;
    int max = -1;
    int max_index = -1;
    int counter = 0;

    cout << "Enter a number: ";
    cin >> num;
    counter++;

    while (num > 0)
    {
        if (max < num)
        {
            max = num;
            max_index = counter;
        }
        cout << "Enter a number: ";
        cin >> num;
        counter++;
    }
    if (max > 0)

    {
        cout << "The max at: "<< max_index << " = " << max << endl;
    }
    else {
        cout << "No valid input data" << endl;
    }
}
