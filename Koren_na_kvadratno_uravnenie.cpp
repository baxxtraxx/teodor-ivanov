#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int a, b, c;
    double D;
    double x1, x2;

    cout << "Enter a, b, c for a*x*x + b*x + c = 0" << endl;
    cin >> a >> b >> c;

    D = b*b - 4*a*c;

    if (D == 0) {
        cout << "No real roots" << endl;
    }
    else if (D < 0) {
        x1 = -b/(2.*a);
        x2 = x1;
        cout << "Single root: " << x1;
    } else
    {
        x1 = (-b + sqrt(D))/(2.*a);
        x2 = (-b - sqrt(D))/(2.*a);
        cout << "x1: " << x1 << " " << "x2: " << x2;
    }
}
