#include <iostream>
#include <vector>

using namespace std;

vector<int> merge(const vector<int> &a, const vector<int> &b);
void prn(const vector<int> &v);
int main()
{
    int n, m;
    cin >> n;
    vector<int> a(n);
    for(int i=0; i<n; i++)
        cin >> a[i];

    cin >> m;
    vector<int> b(m);
    for(int i=0; i<m; i++)
        cin >> b[i];

    vector<int> result = merge(a, b);
    prn(result);
    return 0;
}

vector<int> merge(const vector<int> &a, const vector<int> &b)
{
    vector<int> result;
    int i=0, j=0;
    while(i < a.size() && j < b.size())
    {
        if(a[i] < b[j])
        {
            result.push_back(a[i]);
            i++;
        }
        else
        {
            result.push_back(b[j]);
            j++;
        }
    }
    for(i; i<a.size(); i++) result.push_back(a[i]);
    for(j; j<b.size(); j++) result.push_back(b[j]);
    return result;
}
void prn(const vector<int> &v)
{
    for(int i=0; i<v.size(); i++)
        cout << v[i] << " ";
    cout << endl;
}
