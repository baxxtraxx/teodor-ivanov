#include <iostream>
using namespace std;

bool e_visokosna (int year)

{
    if(year%400 == 0)
    {
        return true;
    }
    if(year%100 ==0)
    {
        return false;
    }
    if(year%4 == 0)
    {
        return true;
    }
    return false;

}

int main()
{
    int year;
    while(cin>>year)
    {
        if(e_visokosna(year))
        {
            cout<< "YES"<<endl;
        }
        else
        {
            cout<< "NO"<<endl;;
        }
    }
    return 0;
}
