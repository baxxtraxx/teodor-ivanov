#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int countChars(const string &filename);
int countWords(const string &filename);
int countLines(const string &filename);

int main()
{
	string filename;
	cin >> filename;
	
	cout << "chars: " << countChars(filename) << endl;
	cout << "words: " << countWords(filename) << endl;
	cout << "lines: " << countLines(filename) << endl;

	//system("pause");
	return 0;
}

int countChars(const string &filename)
{
	ifstream fin(filename);

	char c;
	int cnt = 0;
	while (!fin.eof())
	{
		cnt++;
		char c = fin.get();
	}
	return cnt;
}

int countWords(const string &filename)
{
	ifstream fin(filename);

	string word;
	int cnt = 0;
	while (fin >> word)
	{
		cnt++;
	}
	return cnt;
}

int countLines(const string &filename)
{
	ifstream fin(filename);

	string line;
	int cnt = 0;
	while (getline(fin, line))
	{
		cnt++;
	}
	return cnt;
}