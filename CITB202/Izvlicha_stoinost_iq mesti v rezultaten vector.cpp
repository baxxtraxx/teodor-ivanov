#include <iostream>
#include <vector>
using namespace std;

void print_vector(vector<double> &A)
{
	for (int i = 0; i < A.size(); i++)
	{
		cout << A[i] << " ";
	}
	cout << endl;
}

vector<double> values_at(vector<double> &values, vector<int> &positions)
{
	vector<double> result;

	for (int i = 0; i < positions.size(); i++)
	{
		double value = values[positions[i]];
		result.push_back(value);
	}

	return result;
}


int main()
{
	int N;
	cin >> N;
	vector<double> A(N);
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	int M;
	cin >> M;
	vector<int> B(M);
	for (int i = 0; i < M; i++)
	{
		cin >> B[i];
	}

	vector<double> result = values_at(A, B);
	print_vector(result);

	return 0;
}