#include <iostream>
#include <vector>

using namespace std;

vector<int> factorize(const vector<int> &primes, int n);
void sieve(vector<int> &primes, int maxNumber);
//bool is_prime(const vector<int> &primes, int number);

int main()
{
    int n;
    vector<int> queries;
    int maxn = -1;
    while(cin >> n)
    {
        queries.push_back(n);
        maxn = max(n, maxn);
    }


    vector<int> primes;
    sieve(primes, maxn);

    for(int i=0; i<queries.size(); i++)
    {
        vector<int> factors = factorize(primes, queries[i]);
        cout << queries[i] << " = ";
        for(int i=0; i<factors.size(); i++)
        {
            cout << factors[i];
            if(i < factors.size() - 1)
                cout << " * ";
        }
        cout << endl;
    }
    return 0;
}

void sieve(vector<int> &primes, int maxNumber)
{
    vector<int> m(maxNumber, 0);
    for(int n=2; n<maxNumber; n++)
    {
        if(!m[n])
        {
            primes.push_back(n);
            for(int i=n+n; i<=maxNumber; i+=n)
            {
                m[i]=1;
            }
        }
    }
}

vector<int> factorize(const vector<int> &primes, int n)
{
    vector<int> result;
    int i=0;
    while(n != 1)
    {
        //cout << "i:" << i << ", n:" << n << ", p:" << primes[i] << endl;
        if(n % primes[i] == 0)
        {
            n /= primes[i];
            result.push_back(primes[i]);
            //cout << "n:" << n << endl;
        }
        else
            i++;
    }

    return result;
}
