#include <iostream>
#include <vector>
#include <ctime>
#include <cmath>
#include <memory.h>

using namespace std;

void generate_primes(vector<int> &primes, int maxNumber);
void eratosten(vector<int> &primes, int maxNumber);
void atkin(vector<int> &result, int maxNumber);
bool is_prime(const vector<int> &primes, int number);
void prn(const char * alg, int n, time_t startTime, time_t endTime);
void prn(const vector<int> &primes);
int main()
{
    int n;
    cin >> n;
    vector<int> primes;
    clock_t  startt, endt;
    startt = clock();
    generate_primes(primes, n);
    endt = clock();
    prn("trivial", n, startt, endt);

    vector<int> primes2;
    startt = clock();
    eratosten(primes2, n);
    endt = clock();
    prn("eratosten", n, startt, endt);

    //prn(primes);
    //prn(primes2);


    return 0;
}
void prn(const char * alg, int n, clock_t startTime, clock_t endTime)
{
    cout << alg << " (" << n << "): " << double(endTime - startTime) / CLOCKS_PER_SEC << " seconds." << endl;
}

void prn(const vector<int> &primes)
{
    for(int i=primes.size()-4; i<primes.size(); i++)
    {
        cout << primes[i] << " ";
    }
    cout << endl;
}

void generate_primes(vector<int> &primes, int maxNumber)
{
    if(primes.size() == 0)
        primes.push_back(2);

    for(int n = primes.back()+1; n<maxNumber; n++)
    {
        if(is_prime(primes, n))
            primes.push_back(n);
    }
    return;
}

void eratosten(vector<int> &primes, int maxNumber)
{
    vector<int> m(maxNumber, 0);
    for(int n=2; n<maxNumber; n++)
    {
        if(!m[n])
        {
            primes.push_back(n);
            for(int i=n+n; i<=maxNumber; i+=n)
            {
                m[i]=1;
            }
        }
    }
}

bool is_prime(const vector<int> &primes, int number)
{
    for(int i=0; primes[i]*primes[i] < number && i<primes.size(); i++)
    {
        if(number % primes[i] == 0)
            return false;
    }

    return true;
}
