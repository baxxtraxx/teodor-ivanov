#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(void)
{
	int n;
	cin >> n;
	vector<vector<int>> relations(n + 1);
	vector<string> products(n + 1);
	for (int i = 1; i <= n; i++)
	{
		cin >> products[i];
	}

	int m;
	cin >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		relations[u].push_back(v);
		relations[v].push_back(u);
	}

	int q;
	while (cin >> q)
	{
		cout << products[q];
		int relationsCount = relations[q].size();
		if (relationsCount == 0)
			cout << " has no relations";
		else
			cout << " is related to ";

		for (int i = 0; i < relationsCount; i++)
		{
			int id = relations[q][i];
			cout << products[id];

			if (i < relationsCount - 1)
				cout << ", ";
		}
		cout << endl;
	}

	return 0;
}

