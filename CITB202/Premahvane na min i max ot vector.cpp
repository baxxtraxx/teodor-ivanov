#include <iostream>
#include <algorithm>
#include <vector>
#include <limits>
using namespace std;

void print_vector(const vector<int> &v)
{
    for(int i =0;i<v.size();i++)
    {
        cout<< v[i] << " ";
    }
    cout<< endl;
}

double average_excep_min_and_max(const vector <double> &v)
{
    double max_value = numeric_limits<double>::min();
    double min_value = numeric_limits<double>::min();
    for(int i =0;i<v.size();i++)
    {
        min_value = min(v[i], min_value);
		max_value = max(v[i], max_value);
    }
    double result = 0;
    double cnt = 0;

    for(int i = 0;i<v.size();i++)
    {
        if (v[i] == min_value || v[i] == max_value)
		{
			continue;
		}
		result += v[i];
		cnt++;
    }
    result /= cnt;
    return result;
}

int main()
{
    int n;
    cin>>n;
    vector<double> data(n);
    for(int i = 0;i<n;i++)
    {
        cin>>data[i];
    }
    double average = average_excep_min_and_max(data);
    cout<< average<<endl;
    return 0;
}
