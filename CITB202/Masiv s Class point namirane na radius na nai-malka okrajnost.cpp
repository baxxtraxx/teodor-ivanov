#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

class Point
{
private:
	double x, y;
public:
	Point(double x = 0.0, double y = 0.0) : x(x), y(y) {}

	double get_x()
	{
		return x;
	}

	void set_x(double _x)
	{
		x = _x;
	}

	double get_y()
	{
		return y;
	}

	void set_y(double _y)
	{
		y = _y;
	}

	double distance(const Point &p)
	{
		return sqrt(pow(x - p.x, 2) + pow(y - p.y, 2));
	}
};

int main() {

	int n;
	double x, y;

	cin >> n;

	vector<Point> points;
	for (int i = 0; i < n; i++)
	{
		cin >> x >> y;
		points.push_back(Point(x, y));
	}

	cin >> x >> y;
	Point pivot(x, y);


	/*for (int i = 0; i < points.size(); i++)
	{
		cout << "(" << points[i].get_x() << "," << points[i].get_y() << "):" << pivot.distance(points[i]) << endl;
	}*/

	double best = -1;
	for (int i = 0; i < points.size(); i++)
	{
		double dist = pivot.distance(points[i]);
		best = max(dist, best);
	}

	cout << best << endl;

	return 0;
}
