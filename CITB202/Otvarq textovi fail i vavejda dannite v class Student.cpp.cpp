#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>

using namespace std;

class Student
{
private:
	string facultyNumber, firstName, lastName;
	vector<double> grades;
public:
	Student(const string &facultyNumber, const string & firstName, const string & lastName) : facultyNumber(facultyNumber), firstName(firstName), lastName(lastName) { }

	const string & getFirstName() const
	{
		return firstName;
	}

	const string & getLastName() const
	{
		return lastName;
	}

	const string & getFacultyNumber() const
	{
		return facultyNumber;
	}

	void addGrade(double grade)
	{
		grades.push_back(grade);
	}

	double getAvgGrade()
	{
		double sum = 0;
		for (int i = 0; i < grades.size(); i++)
			sum += grades[i];
		return sum / grades.size();
	}

	const vector<double> & getGrades() const
	{
		return grades;
	}
};

int main()
{
	string infileName, outfileName;
	cin >> infileName;
	cin >> outfileName;
	//filename = "input.txt";

	vector<Student*> students;
	ifstream fin(infileName);
	if (fin.fail())
	{
		cout << "Error opening input file." << endl;
		return -1;
	}

	string facultyNumber, fname, lname;
	while (fin >> facultyNumber >> fname >> lname)
	{
		Student * s = new Student(facultyNumber, fname, lname);
		int gradesCount;
		fin >> gradesCount;
		for (int i = 0; i < gradesCount; i++)
		{
			double grade;
			fin >> grade;
			s->addGrade(grade);
		}
		students.push_back(s);
	}
	fin.close();

	ofstream fout(outfileName);
	if (fout.fail())
	{
		cout << "Error opening output file." << endl;
		return -2;
	}
	fout << fixed << setprecision(2);
	fout << setw(10) << left << "Fac. No" << setw(16) << "First Name" << setw(16) << "Last Name" << setw(6) << right << "Avg" << endl;
	for (int i = 0; i < students.size(); i++)
	{
		Student *s = students[i];
		fout << setw(10) << left << s->getFacultyNumber() << setw(16) << s->getFirstName() << setw(16) << s->getLastName() << setw(6) << right << s->getAvgGrade() << endl;
	}
	fout.flush();
	fout.close();
	//system("pause");
	return 0;
}