#include <iostream>
#include <vector>
using namespace std;

void print_vector(vector<double> &A)
{
	for (int i = 0; i < A.size(); i++)
	{
		cout << A[i] << " ";
	}
	cout << endl;
}

void update_range(vector<double> &A, int start_idx, int end_idx, double value)
{
	for (int i = start_idx; i <= end_idx; i++)
	{
		A[i] += value;
	}
}

int main()
{
	int N;
	cin >> N;
	vector<double> A(N);
	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	int begin, end;
	double value;
	cin >> begin >> end >> value;
	update_range(A, begin, end, value);
	print_vector(A);

	return 0;
}