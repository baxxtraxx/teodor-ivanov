#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Product
{
private:
	double price;
	string name;
public:
	Product(string name, double price) : price(price), name(name) {}

	double get_price()
	{
		return price;
	}

	string get_name()
	{
		return name;
	}
};

/*Synced vectors*/
double get_price(string &product, vector<string> &names, vector<double> &prices);
string get_max(vector<string> &names, vector<double> &prices);
string get_min(vector<string> &names, vector<double> &prices);

/*OOP*/
double get_price(string &product, vector<Product> &products);
string get_max(vector<Product> &products);
string get_min(vector<Product> &products);

int main() {
	int n;
	cin >> n;
	vector<Product> products;
	for (int i = 0; i < n; i++)
	{
		string name;
		double price;
		cin >> name >> price;
		Product p(name, price);
		products.push_back(p);
	}

	string cmd, arg;
	while (cin >> cmd)
	{
		if (cmd == "price")
		{
			cin >> arg;
			double ans = get_price(arg, products);
			if (ans < 0)
				cout << "not found" << endl;
			else
				cout << ans << endl;

		}
		else if (cmd == "max")
		{
			cout << get_max(products) << endl;
		}
		else if (cmd == "min")
		{
			cout << get_min(products) << endl;
		}
	}

	return 0;
}

int main_obsolete() {
	int n;
	cin >> n;
	vector<string> names;
	vector<double> prices;
	for (int i = 0; i < n; i++)
	{
		string name;
		double price;
		cin >> name >> price;
		names.push_back(name);
		prices.push_back(price);
	}

	string cmd, arg;
	while (cin >> cmd)
	{
		if (cmd == "price")
		{
			cin >> arg;
			double ans = get_price(arg, names, prices);
			if (ans < 0)
				cout << "not found" << endl;
			else
				cout << ans << endl;

		}
		else if (cmd == "max")
		{
			cout << get_max(names, prices) << endl;
		}
		else if (cmd == "min")
		{
			cout << get_min(names, prices) << endl;
		}
	}

	return 0;
}

double get_price(string &product, vector<string> &names, vector<double> &prices)
{
	for (int i = 0; i < names.size(); i++)
	{
		if (names[i] == product)
			return prices[i];
	}
	return -1.0;
}
string get_max(vector<string> &names, vector<double> &prices)
{
	int bestIdx = 0;
	double bestPrice = prices[bestIdx];
	for (int i = 1; i < prices.size(); i++)
	{
		if (bestPrice < prices[i])
		{
			bestPrice = prices[i];
			bestIdx = i;
		}
	}

	return names[bestIdx];
}
string get_min(vector<string> &names, vector<double> &prices)
{
	int bestIdx = 0;
	double bestPrice = prices[bestIdx];
	for (int i = 1; i < prices.size(); i++)
	{
		if (bestPrice > prices[i])
		{
			bestPrice = prices[i];
			bestIdx = i;
		}
	}

	return names[bestIdx];
}

/*OOP*/
double get_price(string &product, vector<Product> &products)
{
	for (int i = 0; i < products.size(); i++)
	{
		if (products[i].get_name() == product)
			return products[i].get_price();
	}
	return -1.0;
}
string get_max(vector<Product> &products)
{
	int bestIdx = 0;
	double bestPrice = products[bestIdx].get_price();
	for (int i = 1; i < products.size(); i++)
	{
		if (bestPrice < products[i].get_price())
		{
			bestPrice = products[i].get_price();
			bestIdx = i;
		}
	}

	return products[bestIdx].get_name();
}
string get_min(vector<Product> &products)
{
	int bestIdx = 0;
	double bestPrice = products[bestIdx].get_price();
	for (int i = 1; i < products.size(); i++)
	{
		if (bestPrice > products[i].get_price())
		{
			bestPrice = products[i].get_price();
			bestIdx = i;
		}
	}

	return products[bestIdx].get_name();
}
