#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

class Person {
private:
	string name;
	Person* bestFriend;
	int popularity;

	void increasePopularity()
	{
		this->popularity++;
	}

public:
	Person(const string &name)
	{
		this->name = name;
	}

	void setBestFriend(Person* p)
	{
		this->bestFriend = p;
		if (p != NULL)
			p->increasePopularity();
	}

	const string& getName() const
	{
		return this->name;
	}

	const Person& getBestFriend() const
	{
		return *this->bestFriend;
	}

	const int getPopularity() const
	{
		return this->popularity;
	}
};

Person* findPerson(const vector<Person*>& persons, const string &name)
{
	for (int i = 0; i < persons.size(); i++)
	{
		if (persons[i]->getName() == name)
			return persons[i];
	}
	return NULL;
}

int main()
{
	int N;
	cin >> N;
	vector<Person*> persons(N);
	string name;

	for (int i = 0; i < N; i++)
	{
		cin >> name;

		persons[i] = new Person(name);
	}

	for (int i = 0; i < N; i++)
	{
		cin >> name;

		Person* p = findPerson(persons, name);
		if (p == NULL)
			cout << "Person " << name << " was not recognized." << endl;

		persons[i]->setBestFriend(p);
	}

	for (int i = 0; i < N; i++)
	{
		cout << persons[i]->getName() << " " << persons[i]->getPopularity() << endl;
	}

	return 0;
}
