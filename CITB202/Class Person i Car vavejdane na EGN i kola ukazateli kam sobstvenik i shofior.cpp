#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person
{
private:
	string id, firstName, lastName;
	int age;
public:
	Person(const string &id, const string &fn, const string &ln, int age) : id(id), firstName(fn), lastName(ln), age(age)
	{
	}

	const string & getId() const
	{
		return id;
	}

	int getAge()
	{
		return age;
	}

	void setAge(int age)
	{
		this->age = age;
	}

	const string & getFirstName() const
	{
		return this->firstName;
	}

	const string & getLastName() const
	{
		return this->lastName;
	}
};

class Car
{
private:
	string name;
	Person * owner;
	Person * driver;
public:
	Car(const string &name) : name(name) {}

	void setOwner(Person * owner)
	{
		this->owner = owner;
	}

	Person * getOwner()
	{
		return this->owner;
	}

	void setDriver(Person * driver)
	{
		this->driver = driver;
	}

	Person * getDriver()
	{
		return this->driver;
	}

	const string & getName() const
	{
		return this->name;
	}

	bool driverIsOwner()
	{
		return this->driver == this->owner;
	}
};

void doAge(const vector<Person*> people)
{
	for (int i = 0; i < people.size(); i++)
	{
		people[i]->setAge(people[i]->getAge() + 1);
	}
}

Person * readPerson()
{
	string id, fn, ln;
	int age;
	cin >> id >> fn >> ln >> age;
	return new Person(id, fn, ln, age);
}

Car * readCar()
{
	string name;
	getline(cin, name);
	return new Car(name);
}

Person * findPerson(const vector<Person*> people, const string& id)
{
	for (int i = 0; i < people.size(); i++)
		if (people[i]->getId() == id)
			return people[i];
	return NULL;
}

int main(void)
{
	int n;
	cin >> n;
	cin.ignore(1, '\n');
	vector<Person*> people(n);
	for (int i = 0; i < n; i++)
	{
		people[i] = readPerson();
		string ignore;
	}

	int m;
	cin >> m;
	cin.ignore(1, '\n');
	vector<Car*> cars(m);
	for (int i = 0; i < m; i++)
	{
		cars[i] = readCar();
		string ownerId, driverId;
		cin >> ownerId >> driverId;
		cin.ignore(1, '\n');
		cars[i]->setOwner(findPerson(people, ownerId));
		cars[i]->setDriver(findPerson(people, driverId));
	}

	doAge(people);

	for (int i = 0; i < m; i++)
	{
		Car * c = cars[i];
		cout << c->getName();
		if (c->getOwner() != NULL)
			cout << " is owned by " << c->getOwner()->getLastName() << " (" << c->getOwner()->getAge() << ")";
		else
			cout << " has no owner";

		if (c->getDriver() != NULL)
			cout << " and driven by " << c->getDriver()->getLastName() << " (" << c->getDriver()->getAge() << ")";
		else
			cout << " and has no driver";

		cout << endl;
	}

	return 0;
}

