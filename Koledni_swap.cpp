#include <iostream>
using namespace std;

void swap (double& i, double& j)
{
            int new_value = i;
            i = j;
            j = new_value;
}
int main()
{
    double a = 3,b = 6;
    swap(a,b);
    cout<<a<<" "<<b<<endl;
    return 0;

}
