//Nikola Dimov
//F75329

#include <iostream>
#include <math.h>

using namespace std;

double f(double fArgValue)
{
    //predvaritelno vuvedena funkciq
    //double fValue = fArgValue*fArgValue+10*fArgValue+9;
    //double fValue = fArgValue*fArgValue*fArgValue+3*fArgValue-5;
    double fValue = exp(fArgValue)-2*fArgValue-2;
    return fValue;
}

int main()
{
    double lStep, rStep, step, fLimit, fMismatch;

    cout << "Vuvedete intervala na funkciqta v x: ";
    //vuvejdame lqvata chast na intervala suvpadash s lqvata stupka ot skaniraneto i dqsnata granica na intervala
    cin >> lStep >> fLimit;

    cout << "Vuvedete dopustimoto nesyotvetstvie vuv vrushtanata st-st na f: ";
    cin >> fMismatch;

    //stupkata na skaniraneto e ravna na dva puti po dopustimoto nesuotvetstvie v vrushtanata stoinost
    step = 2*fMismatch;
    //dqsnata stupka priema nachalna stoinost lqva stupka + stupka
    rStep = lStep + step;

    //cikul, vurtqsht se dokato lqvata stupka e po-malka ot dqsnata granica na funkciqta, pri koeto dqsnata stupka v daden moment podminava granicata i taka ne se ispuska proverka
    while(lStep < fLimit)
    {
        //proverka za tova dali funkciqta ima nula se pravi s proverka dali umnojenieto na funkciite ot dqsna i lqva stupka sa po-malki ot nula, t.e. imat li razlichni znaci
        if(f(lStep)*f(rStep)<0)
        {
            //ako tova e taka se izpechatva srednata stoinost na funkciqta v tozi interval kato priblizitelna
            cout << ((lStep+rStep)/2) << endl;
        }
        //ako popadne tochno na korena
        else if(f(lStep)*f(rStep)==0)
        {
            if(f(lStep) == 0)
            {
                cout << "\tTochen koren!" << lStep << endl;
            }
            else
            {
                cout << "\tTochen koren!" << rStep << endl;
            }

            rStep += step; //???propuska edna stupka, za da ne iskara koren i pri sledvashtata proverka
        }

        //lqvata stupka stava dqsna
        lStep = rStep;
        //dqsnata se uvelichava sus stoinostta na stupkata
        rStep += step;
    }
}
