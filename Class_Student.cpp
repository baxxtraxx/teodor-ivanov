#include <string>
#include <iostream>
#include <iomanip>
using namespace std;


class CStudent {
private:
    string m_FName;
    string m_MName;
    string m_LName;

    string m_EGN;
    int    m_Generation;

public:
    void input_data() {
        cout << "What's your first name: ";
        cin >> m_FName;

        cout << "What's your middle name: ";
        cin >> m_MName;

        cout << "What's your last name: ";
        cin >> m_LName;

        cout << "What's your EGN: ";
        cin >> m_EGN;

        cout << "When you started your education: ";
        cin >> m_Generation;
    }

    void print() {
        cout << "Student data: " << endl;
        cout << setw(10) << m_FName;
        cout << setw(10) << m_MName;
        cout << setw(10) << m_LName << endl;
        cout << setw(10) << "EGN: " << setw(10) << m_EGN;
        cout << " Generation: " << setw(6) << m_Generation << endl;
    }

};

int main() {
    CStudent st1;
    CStudent st2;
    CStudent st3;

    st1.input_data();
    st2.input_data();
    st3.input_data();

    st2.print();
    st1.print();
    st3.print();

}
