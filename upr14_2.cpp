#include <iostream>
#include <vector>
using namespace std;

class vec2 {
    public:
        vec2()
        {
            x = 0;
            y = 0;
        }
        vec2(float _x, float _y)
        {
            x = _x;
            y = _y;
        }

        vec2 add(vec2 rhs)
        {
            float resX = x + rhs.x;
            float resY = y + rhs.y;
            vec2 res = vec2(resX, resY);

            return res;
        }

        vec2 add(float rhs)
        {
            float resX = x + rhs;
            float resY = y + rhs;
            vec2 res = vec2(resX, resY);

            return res;
        }

        bool equals(vec2 rhs)
        {
            return x == rhs.x && y == rhs.y;
        }

        //getters
        float getX() { return x; }
        void setX(float _x) { x = _x; }
        float getY() { return y; }
        void setY(float _y) { y = _y; }

    private:
        float x;
        float y;
};

int main()
{
    float x,y;
    cin >> x >> y;
   vec2 a(x , y);
    cin >> x >> y;
   vec2 b(x , y);
   vec2 c = a.add(b);
   cout<< c.getX() << " " << c.getY() << endl;

    //vec2 a(3,5);
    //vec2 b(5,6);

    //vec2 c = a.add(b);
    //vec2 c2 = add(a,b);


    return 0;
}